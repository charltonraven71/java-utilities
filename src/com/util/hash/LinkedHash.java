package com.util.hash;



import java.util.*;

public class LinkedHash<K, V> extends LinkedHashMap<K, ArrayList<V>>
{


    public V add(K key, V value)
    {

        if (!this.containsKey(key)) {

            final V finalValue = value;
            this.put(key, new ArrayList<V>()
            {{
                add(finalValue);
            }});
        } else {

            ArrayList<V> arrayListValue = this.get(key);
            arrayListValue.add(value);
            this.put(key, arrayListValue);
        }
        return null;
    }



    public ArrayList<V> getList(Object key)
    {
        return super.get(key);
    }


    public void sortIntegerValues()
    {
        Set<K> Keys = this.keySet();
        for (K key : Keys) {
            ArrayList<V> list = this.getList(key);
            Collections.sort(list, new Comparator<V>()
            {
                @Override
                public int compare(V o1, V o2)
                {
                    return (Integer) o1 - (Integer) o2;
                }
            });


        }
    }

    public void sortStringValues()
    {
        Set<K> Keys = this.keySet();
        for (K key : Keys) {
            ArrayList<V> list = this.getList(key);
            Collections.sort(list, new Comparator<V>()
            {
                @Override
                public int compare(V o1, V o2)
                {

                    String obj1 = (String) o1;
                    String obj2 = (String) o2;

                    return obj1.compareTo(obj2);

                }
            });


        }
    }

    public ArrayList<K> innerSearch(V SearchValue)
    {
        ArrayList<K> results = new ArrayList<>();
        Set<K> Keys = this.keySet();
        for (K key : Keys) {
            ArrayList<V> Values = this.getList(key);
            for (V Value : Values) {
                if (Value.equals(SearchValue)) {
                    if (!results.contains(key))
                        results.add(key);
                }

            }
        }


        return results;
    }

    public K[] toArray(String... args)
    {


        return null;
    }


}

