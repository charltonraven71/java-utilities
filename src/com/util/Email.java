package com.util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email
{
    private String Host = "";
    private String From = "";
    private String[] To;
    private String[] CC;
    private String Subject = "";
    private String Message = "";

    //-----------------String Message------------------------------------------------------------------------------------------------------------------------------------
    public  Email(String From, String[] to, String[] CC, String Subject, String Message)
    {

        if (!From.equals("")) {
            this.From = From;
        }
        this.To = to;
        if(CC!=null) {
            this.CC = CC;
        }
        this.Subject = Subject;
        this.Message = Message;
    }

    public  Email(String Host, String From, String[] to, String[] CC, String Subject, String Message)
    {
        this.Host = Host;
        if (!From.equals("")) {
            this.From = From;
        }
        this.To = to;
        if(CC!=null){
            this.CC = CC;
        }

        this.Subject = Subject;
        this.Message = Message;

    }
    public  Email(String Host, String From, String to, String CC, String Subject, String Message)
    {
        this.Host = Host;
        if (!From.equals("")) {
            this.From = From;
        }
        this.To = new String[1];
        this.To[0] = to;

        if(CC!=null){
            this.CC = new String[1];
            this.CC[0] = CC;
        }

        this.Subject = Subject;
        this.Message = Message;

    }

    public  Email(String emailFileDir, String Subject, String Message)
    {
        this.Subject = Subject;
        this.Message = Message;
        try {
            File file = new File(emailFileDir);
            Scanner sc = new Scanner(file);
            sc.useDelimiter("\\Z");
            String[] emailArray = sc.next().split("\\|");

            if (!emailArray[0].split(",")[0].equals("")) {
                this.From = emailArray[0].split(",")[0];
            }
            this.To = emailArray[1].split(",");
            if(emailArray.length>2) {
                this.CC = emailArray[2].split(",");
            }

            System.out.println();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public  Email(File emailFileDir, String Subject, String Message)
    {
        this.Subject = Subject;
        this.Message = Message;
        try {

            Scanner sc = new Scanner(emailFileDir);
            sc.useDelimiter("\\Z");
            String[] emailArray = sc.next().split("\\|");

            if (!emailArray[0].split(",")[0].equals("")) {
                this.From = emailArray[0].split(",")[0];
            }
            this.To = emailArray[1].split(",");
            if(emailArray.length>2) {
                this.CC = emailArray[2].split(",");
            }

            System.out.println();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public  Email(String Host, String emailFileDir, String Subject, String Message)
    {
        this.Subject = Subject;
        this.Message = Message;
        this.Host = Host;

        try {
            File file = new File(emailFileDir);
            Scanner sc = new Scanner(file);
            sc.useDelimiter("\\Z");
            String[] emailArray = sc.next().split("\\|");

            if (!emailArray[0].split(",")[0].equals("")) {
                this.From = emailArray[0].split(",")[0];
            }
            this.To = emailArray[1].split(",");
            if(emailArray.length>2) {
                this.CC = emailArray[2].split(",");
            }
            System.out.println();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
//-----------------File Message------------------------------------------------------------------------------------------------------------------------------------

    public  Email(String From, String[] to, String[] CC, String Subject,File Message)
    {

        if (!From.equals("")) {
            this.From = From;
        }
        this.To = to;
        if(CC!=null) {
            this.CC = CC;
        }
        this.Subject = Subject;
        setMessage(Message);
    }

    public  Email(String Host, String From, String[] to, String[] CC, String Subject,File Message)
    {
        this.Host = Host;
        if (!From.equals("")) {
            this.From = From;
        }
        this.To = to;
        if(CC!=null) {
            this.CC = CC;
        }
        this.Subject = Subject;
        setMessage(Message);

    }

    public  Email(String emailFileDir, String Subject,File Message)
    {
        this.Subject = Subject;
        setMessage(Message);
        try {
            File file = new File(emailFileDir);
            Scanner sc = new Scanner(file);
            sc.useDelimiter("\\Z");
            String[] emailArray = sc.next().split("\\|");

            if (!emailArray[0].split(",")[0].equals("")) {
                this.From = emailArray[0].split(",")[0];
            }
            this.To = emailArray[1].split(",");
            if(emailArray.length>2) {
                this.CC = emailArray[2].split(",");
            }

            System.out.println();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public  Email(File emailFileDir, String Subject,File Message)
    {
        this.Subject = Subject;
        setMessage(Message);
        try {

            Scanner sc = new Scanner(emailFileDir);
            sc.useDelimiter("\\Z");
            String[] emailArray = sc.next().split("\\|");

            if (!emailArray[0].split(",")[0].equals("")) {
                this.From = emailArray[0].split(",")[0];
            }
            this.To = emailArray[1].split(",");
            if(emailArray.length>2) {
                this.CC = emailArray[2].split(",");
            }

        } catch (FileNotFoundException ex){
            ex.printStackTrace();
        }
    }

    public  Email(String Host, String emailFileDir, String Subject,File Message)
    {
        this.Subject = Subject;
        setMessage(Message);
        this.Host = Host;

        try {
            File file = new File(emailFileDir);
            Scanner sc = new Scanner(file);
            sc.useDelimiter("\\Z");
            String[] emailArray = sc.next().split("\\|");

            if (!emailArray[0].split(",")[0].equals("")) {
                this.From = emailArray[0].split(",")[0];
            }
            this.To = emailArray[1].split(",");
            if(emailArray.length>2) {
                this.CC = emailArray[2].split(",");
            }

            System.out.println();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void setMessage(File messageFile)
    {

        try {
            this.Message = new String(Files.readAllBytes(Paths.get(messageFile.getAbsolutePath())));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setMessage(String messageFilePath)
    {

        try {
            this.Message = new String(Files.readAllBytes(Paths.get(messageFilePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void Send()
    {
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", Host);

        Session session = Session.getDefaultInstance(properties);

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(From));
            InternetAddress[] mailAddress_To = new InternetAddress[To.length];
            for (int i = 0; i < To.length; i++) {
                mailAddress_To[i] = new InternetAddress(To[i]);
            }
            message.addRecipients(MimeMessage.RecipientType.TO, mailAddress_To);
            if(CC!=null) {
                InternetAddress[] mailAddress_CC = new InternetAddress[CC.length];
                for (int i = 0; i < CC.length; i++) {
                    mailAddress_CC[i] = new InternetAddress(CC[i]);
                }
                message.addRecipients(MimeMessage.RecipientType.CC, mailAddress_CC);
            }

            message.setSubject(Subject);
            message.setText(Message);

            Transport.send(message);
            System.out.println("Sent Message Successfully....");


        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }

    public void setHost(String host)
    {
        Host = host;
    }

    public void setFrom(String from)
    {
        From = from;
    }

    public void setTo(String[] to)
    {
        To = to;
    }

    public void setCC(String[] CC)
    {
        this.CC = CC;
    }

    public void setSubject(String subject)
    {
        Subject = subject;
    }

    public String getHost()
    {
        return Host;
    }

    public String getFrom()
    {
        return From;
    }

    public String[] getTo()
    {
        return To;
    }

    public String[] getCC()
    {
        return CC;
    }

    public String getSubject()
    {
        return Subject;
    }

    public String getMessage()
    {
        return Message;
    }

}
